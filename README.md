Community Test

💯 Bootstrapped Project via `create-react-app`

😎 Using Axios to query data from mini API where posts exist

💪 Plain ole state management 

🔥 Best attempt at keeping atlaskit approved look and feel with scss

To check this out locally...

```bash
git clone https://matt_jared@bitbucket.org/matt_jared/community_test.git
nvm use
npm install
npm run start
``` 
## Bonus points answers
- Changing of urls based on tabs answer: I'd like to import `react-router` and use that to handle the different tabs. [This example](https://reacttraining.com/react-router/web/example/ambiguous-matches) with ambiquous matches would be ideal since we don't _know_ the name of each tab. 
- Responsive all the things! I'd probably rip out the "tab" visuals and go with a collapsed nav where the sidebar items would be below each "tab" and on click it would "activate" the sidebar so you could see posts options below each area. Another option is to simply stack all the items w/o the collapsing effect and tuck posts underneath the tabs.

## Other Things I'd like to do w/ more time
- Submit a PR to atlaskit so there's an scss version of all brand colors
- Rip out PNGs used and inline a single SVG component for the logo that switches colors based on a prop passed
- Figure out auto-showing the first post in a list when a new tab is selected
- Only map over all the posts once and couple the Sidebar and Post content in a cleaner way
- Get some nicer transitions going up in here especially between posts. 
- Super sloppy currently with PropTypes but I'd like to get that cleaned up as well
- Linting, editorconfig and look into flow

