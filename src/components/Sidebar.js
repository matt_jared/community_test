import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
 
class Sidebar extends Component {
  render() {
    let sidebarClass = 'sidebar-item';
    if (this.props.activePost === this.props.title) {
      sidebarClass += ' active';
    }
    return (
      <div className={sidebarClass} onClick={() => this.props.action(this.props.title)}>
        <h5 className="sidebar-item-headline">{this.props.title}</h5>
        <span className="sidebar-item-meta">{this.props.speakers[0].FirstName} {this.props.speakers[0].LastName}, {this.props.speakers[0].Company}</span>
      </div>
    );
  }
}

Sidebar.propTypes = {
	headline: PropTypes.any,
}

export default Sidebar;