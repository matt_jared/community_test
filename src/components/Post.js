import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
 
class Post extends Component {
  render() {
    return (
      <div className="post">
        <h2 className="post-headline">{this.props.title}</h2>
        <p><strong>{this.props.speakers[0].FirstName} {this.props.speakers[0].LastName} </strong></p>
        <p>{this.props.description}</p>
        <p>{this.props.track.Title}</p>
        <h3>About the speaker</h3>
        <p className="post-author"><strong>{this.props.speakers[0].FirstName} {this.props.speakers[0].LastName},</strong> <span> {this.props.speakers[0].Company}</span></p>
        <p>{this.props.speakers[0].Bio}</p>
      </div>
    );
  }
}

Post.propTypes = {
	headline: PropTypes.any,
}

export default Post;