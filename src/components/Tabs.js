import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
 
class Tabs extends Component {
  render() {
    let tabClass = 'tab';
    if (this.props.activeTrack === this.props.track) {
      tabClass += ' active';
    }
    return (
      <div className={tabClass} onClick={() => this.props.action(this.props.track)}>
        {this.props.track}
      </div>
    );
  }
}

Tabs.propTypes = {
	headline: PropTypes.any,
}

export default Tabs;