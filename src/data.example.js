{
  "Room": { 
    "Name": "Golden Gate B",
    "Capacity":"575"
  },
  "Id":"27829",
  "Description":"Tractors, sprayers, and combines have more technology than you think â€“ and they're getting smarter every day. Join this session to learn how cutting-edge technology is being used in a manufacturing company. Diego Lucas will walk through AGCO's three-year journey from a traditional, ad-hoc set of technologies and processes for the development lifecycle to a modern, agile automation-obsessed devops culture, using Atlassian tools in conjunction with technologies such as Heroku and Docker. He'll also discuss cultural aspects of change as well as go into lessons learned and provide a glimpse of what lies ahead.",
  "IsFeatured":"False",
  "TimeSlot": {
    "EndTime": "11/5/2015 7:35:00 PM",
    "Label":"Breakout 7",
    "StartTime":"11/5/2015 6:50:00 PM"
  },
  "Track": {
    "Title":"Build",
    "Description":"Build"
  },
  "LastModified":"9/22/2015 4:07:06 AM",
  "Published":"True",
  "SessionType": 
    {
      "Name":"Breakout"
    },
    "MetadataValues": [
      {
        "Value":"Basic",
        "Details": {
          "Title":"Technical Level ",
          "Options": ["Basic","Intermediate","Advanced"],
          "FieldType":"DropDown"
        }
      }
    ],
    "Title":"Tractors and DevOps: Harvesting the Fruits of Automation",
    "Speakers": [
      {
        "AttendeeID":"bMedMbpxRnZpdXhYv7y8",
        "Biography":"",
        "LastName":"Lucas",
        "EmailAddress":"",
        "Title":"",
        "AvailableforMeeting":"False",
        "FirstName":"Diogo",
        "Industry":"",
        "Roles":[
          "Speaker"
        ],
        "Company":"AGCO",
        "LastUpdated":"9/22/2015 4:07:06 AM","ID":"111417"
      }
    ],
    "Mandatory": "False"
  }
}