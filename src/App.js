import React, { Component } from 'react';
import Post from './components/Post';
import Tabs from './components/Tabs';
import Sidebar from './components/Sidebar';
import axios from 'axios';
import BlueLogo from './blue-bg-logo.png';
import WhiteLogo from './white-bg-logo.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.setupTabs = this.setupTabs.bind(this);
    this.changeTrack = this.changeTrack.bind(this);
    this.changePost = this.changePost.bind(this);
    this.state = { 
      post: [],
      tracks: [],
      activeTrack: 'Build',
      activePost: 'Tractors and DevOps: Harvesting the Fruits of Automation'
    };
  }
  componentDidMount() {
    axios.get('https://bitbucket.org/dcor_atlassian/atlassian-web-developer-code-exercise/raw/master/sessions.json')
    .then(res => {
      const post = res.data.Items;
      this.setState({ post })
      this.setupTabs();
    })
  };
  setupTabs() {
    const allTracks = [];
    this.state.post.map((post, i) => {
      return allTracks.push(post.Track.Title);
    })
    const singleTracks = [ ...new Set(allTracks) ];
    this.setState({
      tracks: singleTracks,
    })
  };
  changeTrack(track) {
    this.setState({
      activeTrack: track,
      activePost: '',
    });
  };
  changePost(post) {
    this.setState({
      activePost: post,
    });
  };
  render() {
    return (
      <div className="App">
        <header className="header">
          <img src={BlueLogo} alt="Logo" height="21px"/>
        </header>
        <div className="container">
          <div className="intro">
            <img src={WhiteLogo} alt="Logo" height="23px"/>
            <h4 className="intro-subtitle">Video Archive</h4>
          </div>
          <div className="tabs">{this.state.tracks.map((track, i) => {
            if(!["Training","Activities"].includes(track)) {
              return (
                <Tabs 
                  key={i}
                  track={track}
                  action={this.changeTrack}
                  activeTrack={this.state.activeTrack}
                />
              )
            }
          })}</div>
          <div className="content-body">
            <div className="content-sidebar">
              <h2 className="sidebar-headline">{ this.state.activeTrack }</h2>
              {this.state.post.map((post, i) => {
                if(this.state.activeTrack === post.Track.Title) {
                  return (
                    <Sidebar 
                      key={i}
                      title={post.Title}
                      speakers={post.Speakers} 
                      action={this.changePost}
                      activePost={this.state.activePost}
                    />
                  )
                }
              })}
            </div>
            <div className="content-post">{this.state.post.map((post, i) => {
              if(this.state.activePost === post.Title) {
                return (
                  <Post 
                    key={post.Id}
                    title={post.Title}
                    description={post.Description}
                    track={post.Track}
                    speakers={post.Speakers} 
                    action={this.changePost}
                  />
                )
              } 
            })}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
